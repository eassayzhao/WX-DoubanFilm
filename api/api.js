const URLS = {
  hotUrl : 'https://m.douban.com/rexxar/api/v2/subject_collection/movie_showing/items',
  latestUrl : 'https://m.douban.com/rexxar/api/v2/subject_collection/movie_latest/items',
  freeUrl : 'https://m.douban.com/rexxar/api/v2/subject_collection/movie_free_stream/items'
}

const loadHotFilms = function(params = {}){
  return new Promise((resolve,reject) => {
    wx.request({
      url: URLS.hotUrl,
      data:params,
      success:resolve,
      fail:reject
    })
  }).then(res => {
    if(res.statusCode == 200) /**服务器请求成功 */
    {
      res.data.method = 'loadHotFilms';
      return res.data;
    }else{
      //将状态从resolve转换为reject
      return Promise.reject({
        message:res.errMsg
      });
    }
  }) ;
}
const loadLatestFilms = function(params = {}){
  return new Promise((resolve,reject) => {
    wx.request({
      url: URLS.latestUrl,
      data:params,
      success:resolve,
      fail:reject
    })
  }).then(res => {
    if(res.statusCode == 200) /**服务器请求成功 */
    {
      res.data.method = 'loadLatestFilms';
      return res.data;
    }else{
      //将状态从resolve转换为reject
      return Promise.reject({
        message:res.errMsg
      });
    }
  }) ;
}
const loadFreeFilms = function(params = {}){
  return new Promise((resolve,reject) => {
    wx.request({
      url: URLS.freeUrl,
      data:params,
      success:resolve,
      fail:reject
    })
  }).then(res => {
    if(res.statusCode == 200) /**服务器请求成功 */
    {
      res.data.method = 'loadFreeFilms';
      return res.data;
    }else{
      //将状态从resolve转换为reject
      return Promise.reject({
        message:res.errMsg
      });
    }
  }) ;
}
//导出模块,对外暴露接口
module.exports ={
  loadHotFilms,
  loadLatestFilms,
  loadFreeFilms
}