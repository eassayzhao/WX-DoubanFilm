// 导入模块
const api = require('../../api/api.js')
Page({

  data: {
    types:[] //存储分类
  },
  onReady: function () {
    this.loadHomeData();
  },
 loadHomeData(){
   //加载影院热映
   api.loadHotFilms({
     start:0,
     count:6
   }).then(data => {
    let type = {
      title:data.subject_collection.name,
      list:data.subject_collection_items,
      method:data.method
    }
    this.setData({
      'types[0]':type
    });
   }).catch(error => {
     wx.showToast({
       title: error.message,
       image:'/imgs/error.png'
     })
   });
   api.loadLatestFilms({
    start:0,
    count:6
  }).then(data => {
   let type = {
     title:data.subject_collection.name,
     list:data.subject_collection_items,
     method:data.method
   }
   this.setData({
     'types[1]':type
   });
  }).catch(error => {
    wx.showToast({
      title: error.message,
      image:'/imgs/error.png'
    })
  });
  api.loadFreeFilms({
    start:0,
    count:6
  }).then(data => {
   let type = {
     title:data.subject_collection.name,
     list:data.subject_collection_items,
     method:data.method
   }
   this.setData({
     'types[2]':type
   });
  }).catch(error => {
    wx.showToast({
      title: error.message,
      image:'/imgs/error.png'
    })
  });
 }
})