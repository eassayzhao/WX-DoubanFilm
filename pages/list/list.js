// 导入模块
const api = require('../../api/api.js');
Page({
  data: {
    method:'', //存储分类
    films:{},//存储分类的电影
    start:0,//起始点
    count:12,//返回数量
    total:0,//记录总数量
    showLoading:false,//是否显示loading
    showNomore:false,//是否显示nomore
  },

  onLoad: function (options) {
    //console.log(options)
    this.data.method = options.method;
  },
  onReady: function (){
    this.loadListData();
  },
  loadListData(){
  // 返回Promise对象
    return api[this.data.method]({
      start:this.data.start,
      count:this.data.count
    }).then( data => {
      let list = this.data.films.list || [];
      let films = {
        title:data.subject_collection.name,
        list:list.concat(data.subject_collection_items)
      }
      this.setData({
        films:films,
        start:this.data.start+this.data.count,//起始索引，每次增加12条
        total:data.total//记录总数量
      });
    });
  },
  onReachBottom:function(){
    //判断是否还有更多的数据
    if(this.data.start < this.data.total){
      this.setData({
        showLoading:true
      });
      this.loadListData()
        .then(() =>{
          //隐藏loading
          this.setData({
            showLoading:false
          });
        });
    }else{
      this.setData({
        showNomore:true
      });
    }
    }    
})
